{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "secretsmanager:GetSecretValue"
      ],
      "Effect": "Allow",
      "Resource": [
        "${secret-arn}"
      ]
    },
    {
      "Action": [
        "kms:Decrypt"
      ],
      "Effect": "Allow",
      "Resource": [
        "${kms-key-arn}"
      ],
      "Condition": {
        "StringEquals": {
          "kms:ViaService": "secretsmanager.${region}.amazonaws.com"
        }
      }
    }
  ]
}
