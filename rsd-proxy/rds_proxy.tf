
resource "aws_db_proxy" "pe_db_proxy_policy_db_proxy" {
  name                   = "${local.datastore_db_name}-rds-proxy"
  debug_logging          = true
  engine_family          = "MYSQL"
  idle_client_timeout    = 1800
  require_tls            = false
  role_arn               = aws_iam_role.db_proxy_policy.arn
  vpc_security_group_ids = aws_rds_cluster.pe_aurora_network.vpc_security_group_ids
  vpc_subnet_ids         = aws_db_subnet_group.pe_aurora_subnet.subnet_ids

  auth {
    auth_scheme = "SECRETS"
    description = "allows the connection to the aurora db"
    iam_auth    = "DISABLED"
    secret_arn  = aws_secretsmanager_secret.rds_username_and_password.arn
  }

  tags = {
    Name = "aurora proxy"
  }
}

resource "aws_db_proxy_default_target_group" "pe_db_proxy_policy_db_proxy" {
  db_proxy_name = aws_db_proxy.pe_db_proxy_policy_db_proxy.name

  connection_pool_config {
    connection_borrow_timeout    = 120
    init_query                   = "SET x=1, y=2"
    max_connections_percent      = 100
    max_idle_connections_percent = 50
    session_pinning_filters      = ["EXCLUDE_VARIABLE_SETS"]
  }
}

resource "aws_db_proxy_target" "pe_db_proxy_policy_db_proxy" {
  db_cluster_identifier  = aws_rds_cluster.pe_aurora_network.cluster_identifier
  db_proxy_name          = aws_db_proxy.pe_db_proxy_policy_db_proxy.name
  target_group_name      = aws_db_proxy_default_target_group.pe_db_proxy_policy_db_proxy.name
}