
locals {
  datastore_db_name = "pe-aurora-datastore"
}

provider "aws" {
  region = var.aws_region
}

module "pe_aurora_network_sg" {
  source = "terraform-aws-modules/security-group/aws"
  version = "3.18.0"

  name = "${local.datastore_db_name}-sg"
  use_name_prefix = false
  description = "Security group for Aurora serverless ${local.datastore_db_name}"
  vpc_id = aws_vpc.vpc1.id

  ingress_with_cidr_blocks = [
    {
      rule        = "mysql-tcp"
      description  = "VPN"
      cidr_blocks = "172.26.0.0/16,10.96.0.0/16,10.94.0.0/16,10.95.0.0/16,172.18.1.0/24"
    },
    {
      rule        = "mysql-tcp"
      description  = "VPC"
      cidr_blocks = aws_vpc.vpc1.cidr_block
    }
  ]

  tags = {
      "Name" = "${local.datastore_db_name}-sg"
      "nombre" = "${local.datastore_db_name}-sg"
    }
}