resource "aws_db_subnet_group" "pe_aurora_subnet" {
  name = "${local.datastore_db_name}-subnet-group"
  subnet_ids = [aws_subnet.subnet1.id, aws_subnet.subnet2.id]

  tags = {
      "Name" = "${local.datastore_db_name}-subnet-group"
      "nombre" = "${local.datastore_db_name}-subnet-group"
    }
}