provider "aws" {
  region = "us-east-1"
}
module "consul" {
  source = "hashicorp/consul/aws"
  version = "0.11.0"
}