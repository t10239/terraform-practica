provider "aws" {
  region = "us-east-1"
}

module "vpc" {
  source = "./vpc"

  // Variables input type
  vpc_cidr = "19.82.0.0/16"
  vpc_name = "vpc_module1"

  subnet_name = "Subnet1a"
  subnet_cidr = "19.82.1.0/24"
  az = "us-east-1a"

  ec2_private_ip=["19.82.1.82"]
}

module "ec2" {
  source = "./ec2"

  // Variables input type
  ec2ami = "ami-0022f774911c1d690"
  ec2type = "t3.micro"
  ec2iface = module.vpc.ec2_network_interface
  ec2name = "EC2 name from Module"
}
